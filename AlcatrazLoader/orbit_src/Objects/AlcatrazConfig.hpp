#pragma once

#include "pch.h"

namespace AlcatrazUplayR2
{
	struct AlcatrazConfig
	{
		Variant dataVariant;
		bool borderlessWindow = true;
		bool discordRichPresence = true;
	};

} // namespace AlcatrazUplayR2
